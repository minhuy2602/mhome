package com.demo.architect.employee.app.di;

/**
 * Created by uyminhduc on 12/16/16.
 */

public interface HasComponent<C> {
    C getComponent();
}
