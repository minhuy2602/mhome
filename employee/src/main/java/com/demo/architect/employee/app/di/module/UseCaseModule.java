package com.demo.architect.employee.app.di.module;

import com.demo.architect.data.repository.base.local.LocalRepository;
import com.demo.architect.data.repository.bill.remote.BillRepository;
import com.demo.architect.domain.usecase.demo.IMDBUsecase;

import dagger.Module;
import dagger.Provides;

/**
 * Created by uyminhduc on 12/16/16.
 */
@Module
public class UseCaseModule {
    public UseCaseModule() {
    }

    @Provides
    IMDBUsecase provideIMDBUsecase(BillRepository mApiRepository, LocalRepository localRepository) {
        return new IMDBUsecase(mApiRepository, localRepository);
    }

}

