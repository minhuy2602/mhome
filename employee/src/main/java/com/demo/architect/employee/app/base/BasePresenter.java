package com.demo.architect.employee.app.base;

/**
 * Created by uyminhduc on 12/16/16.
 */

public interface BasePresenter {
    void start();
    void stop();
}
