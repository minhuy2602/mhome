package com.demo.architect.domain.usecase.demo;

import com.demo.architect.data.repository.base.remote.RemoteRepository;
import com.demo.architect.domain.usecase.BaseUseCase;

import rx.Observable;

/**
 * Created by admin on 10/3/17.
 */

public class TurnOnLightUsecase extends BaseUseCase {
    private final RemoteRepository remoteRepository;

    public TurnOnLightUsecase(RemoteRepository remoteRepository) {
        this.remoteRepository = remoteRepository;
    }

    @Override
    protected Observable buildUseCaseObservable() {
        return remoteRepository.turnOnLight();
    }
}
