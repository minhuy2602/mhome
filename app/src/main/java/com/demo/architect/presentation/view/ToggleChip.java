package com.demo.architect.presentation.view;

import android.content.Context;
import android.util.AttributeSet;

public class ToggleChip extends android.support.v7.widget.AppCompatCheckedTextView {


  public ToggleChip(Context context, AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);
  }

  public ToggleChip(Context context, AttributeSet attrs) {
    super(context, attrs);
  }

  public ToggleChip(Context context) {
    super(context);
  }

  public void setLabel(String label){
    setText(label);
  }
}
