package com.demo.architect.presentation.screen.wrapper;

/**
 * Created by admin on 4/25/17.
 */

public class CouponWrapperItem {

    public final static int TYPE_ITEM = 0;
    public final static int TYPE_NUMBER = 1;

    private String thumb;
    private String title;
    private String id;
    private String number;
    private int type;

    public CouponWrapperItem(String thumb, String title, String id) {
        this.thumb = thumb;
        this.title = title;
        this.id = id;
        this.type = TYPE_ITEM;
    }

    public CouponWrapperItem(String number) {
        this.number = number;
        this.type = TYPE_NUMBER;
    }

    public static CouponWrapperItem createNumberCouponWrapperItem(String number) {
        return new CouponWrapperItem(number);
    }

    public static CouponWrapperItem createCouponWrapperItem(String thumb, String title, String id) {
        return new CouponWrapperItem(thumb, title, id);
    }

    public String getThumb() {
        return thumb;
    }

    public void setThumb(String thumb) {
        this.thumb = thumb;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getNumber() {
        return number;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }
}
