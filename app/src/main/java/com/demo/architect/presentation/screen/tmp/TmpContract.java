package com.demo.architect.presentation.screen.tmp;

import com.demo.architect.presentation.app.base.BasePresenter;
import com.demo.architect.presentation.app.base.BaseView;

/**
 * Created by admin on 7/14/17.
 */

public class TmpContract {
    interface View extends BaseView<Presenter> {
        void ConvertTextToSpeech(String content);
    }

    interface Presenter extends BasePresenter {
        void turnOnLight();

        void turnOffLight();

        void turnOnMusic();

        void turnOffMusic();

        void turnOnRem();

        void turnOffRem();
    }
}
