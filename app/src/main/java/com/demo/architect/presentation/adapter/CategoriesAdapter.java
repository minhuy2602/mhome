package com.demo.architect.presentation.adapter;


import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.demo.architect.presentation.R;
import com.demo.architect.presentation.screen.wrapper.CategoryWrapperItem;

import java.util.List;

/**
 * Created on 4/21/2017.
 */

public class CategoriesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<CategoryWrapperItem> mCategories;

    public CategoriesAdapter(List<CategoryWrapperItem> categories) {
        this.mCategories = categories;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        switch (viewType) {
            case CategoryViewHolder.CategoryType.HEADER:
                View header = inflater.inflate(R.layout.layout_header, parent, false);
                return new HeaderViewHolder(header);
            default:
                View row = inflater.inflate(R.layout.layout_category_item, parent, false);
                return new HeaderViewHolder(row);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

    }

    @Override
    public int getItemViewType(int position) {
        return position == 0 ? CategoryViewHolder.CategoryType.HEADER : CategoryViewHolder.CategoryType.ROW;
    }

    @Override
    public int getItemCount() {
        return mCategories.size();
    }
}
