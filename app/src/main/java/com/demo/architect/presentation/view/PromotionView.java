package com.demo.architect.presentation.view;

import android.content.Context;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.AttributeSet;
import android.view.View;
import android.widget.RelativeLayout;

import com.demo.architect.presentation.R;
import com.demo.architect.presentation.adapter.PromotionAdapter;
import com.demo.architect.presentation.screen.wrapper.PromotionWrapperItem;

import java.util.ArrayList;

/**
 * Created by admin on 4/25/17.
 */

public class PromotionView extends RelativeLayout {

    private RecyclerView rvPromotionList;

    private ArrayList<PromotionWrapperItem> promotionList;
    private PromotionAdapter promotionAdapter;

    public PromotionView(Context context) {
        super(context);
        initViews();
    }

    public PromotionView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initViews();
    }

    public PromotionView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initViews();
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public PromotionView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        initViews();
    }

    private void initViews() {
        View view = inflate(getContext(), R.layout.view_list_promotion, this);

        promotionList = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            promotionList.add(PromotionWrapperItem.createPromotionWrapperItem("","","",""));
        }

        rvPromotionList = (RecyclerView) view.findViewById(R.id.rv_promotion_list);

        promotionAdapter = new PromotionAdapter(promotionList);
        StaggeredGridLayoutManager staggeredGridLayoutManager = new StaggeredGridLayoutManager(1, LinearLayoutManager.HORIZONTAL);
        rvPromotionList.setLayoutManager(staggeredGridLayoutManager);
        rvPromotionList.setAdapter(promotionAdapter);
        promotionAdapter.notifyDataSetChanged();
    }
}
