package com.demo.architect.presentation.screen.wrapper;

/**
 * Created by admin on 4/25/17.
 */

public class PromotionWrapperItem {

    private String title;
    private String shortDescription;
    private String thumb;
    private String id;

    public PromotionWrapperItem(String title, String shortDescription, String thumb, String id) {
        this.title = title;
        this.shortDescription = shortDescription;
        this.thumb = thumb;
        this.id = id;
    }

    public static PromotionWrapperItem createPromotionWrapperItem(String title, String shortDescription, String thumb, String id) {
        return new PromotionWrapperItem(title, shortDescription, thumb, id);
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getShortDescription() {
        return shortDescription;
    }

    public void setShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
    }

    public String getThumb() {
        return thumb;
    }

    public void setThumb(String thumb) {
        this.thumb = thumb;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
