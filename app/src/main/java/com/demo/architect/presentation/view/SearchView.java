package com.demo.architect.presentation.view;

import android.content.Context;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.demo.architect.presentation.R;
import com.demo.architect.presentation.adapter.SearchFilterAdapter;
import com.demo.architect.presentation.screen.wrapper.SearchFilterWrapperItem;

import java.util.ArrayList;

/**
 * Created by admin on 5/4/17.
 */

public class SearchView extends RelativeLayout implements SearchFilterAdapter.SearchFilterItemListener {

    private RecyclerView rvSearchList;
    private ArrayList<SearchFilterWrapperItem> items;
    private SearchFilterAdapter searchFilterAdapter;
    private TextView tvSearchFilterTitle;

    public SearchView(Context context) {
        super(context);
        initView();
    }

    public SearchView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView();
    }

    public SearchView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView();
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public SearchView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        initView();
    }

    private void initView() {
        View view = inflate(getContext(), R.layout.view_list_search_filter, this);

        tvSearchFilterTitle = (TextView) findViewById(R.id.tv_search_filter_title);
        rvSearchList = (RecyclerView) findViewById(R.id.rv_search_filter_list);

        items = new ArrayList<>();

        for (int i = 0; i < 5; i++) {
            items.add(SearchFilterWrapperItem.createSearchFilterWrapperItem(R.drawable.ic_label, "", ""));
        }
        items.add(SearchFilterWrapperItem.createSearchFilterLoadMoreWrapperItem(""));

        searchFilterAdapter = new SearchFilterAdapter(items, this);
        rvSearchList.setLayoutManager(new LinearLayoutManager(getContext()));
        rvSearchList.setAdapter(searchFilterAdapter);
        searchFilterAdapter.notifyDataSetChanged();
    }
}
