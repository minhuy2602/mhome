package com.demo.architect.presentation.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created on 4/21/2017.
 */

class HeaderViewHolder extends RecyclerView.ViewHolder {

    public class CategoryType {
        public static final int HEADER = 1;
        public static final int ROW = 2;
    }

    public HeaderViewHolder(View root) {
        super(root);
    }
}
