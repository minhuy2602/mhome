package com.demo.architect.presentation.screen.tmp;

import android.support.annotation.NonNull;
import android.util.Log;

import com.demo.architect.domain.usecase.demo.TurnOffLightUsecase;
import com.demo.architect.domain.usecase.demo.TurnOffMusicUsecase;
import com.demo.architect.domain.usecase.demo.TurnOffRemUsecase;
import com.demo.architect.domain.usecase.demo.TurnOnLightUsecase;
import com.demo.architect.domain.usecase.demo.TurnOnMusicUsecase;
import com.demo.architect.domain.usecase.demo.TurnOnRemUsecase;

import javax.inject.Inject;

import rx.Subscriber;

/**
 * Created by admin on 7/14/17.
 */

public class TmpPresenter implements TmpContract.Presenter {

    private final static String TAG = TmpPresenter.class.getName();

    private TmpContract.View view;
    private TurnOnLightUsecase turnOnLightUsecase;
    private TurnOffLightUsecase turnOffLightUsecase;
    private TurnOnMusicUsecase turnOnMusicUsecase;
    private TurnOffMusicUsecase turnOffMusicUsecase;
    private TurnOnRemUsecase turnOnRemUsecase;
    private TurnOffRemUsecase turnOffRemUsecase;

    @Inject
    public void setupPresenter() {
        view.setPresenter(this);
    }

    @Override
    public void start() {

    }

    @Override
    public void stop() {

    }

    @Inject
    public TmpPresenter(TmpContract.View view,
                        @NonNull TurnOnLightUsecase turnOnLightUsecase,
                        @NonNull TurnOffLightUsecase turnOffLightUsecase,
                        @NonNull TurnOnMusicUsecase turnOnMusicUsecase,
                        @NonNull TurnOffMusicUsecase turnOffMusicUsecase,
                        @NonNull TurnOnRemUsecase turnOnRemUsecase,
                        @NonNull TurnOffRemUsecase turnOffRemUsecase) {
        this.view = view;
        this.turnOnLightUsecase = turnOnLightUsecase;
        this.turnOffLightUsecase = turnOffLightUsecase;
        this.turnOnMusicUsecase = turnOnMusicUsecase;
        this.turnOffMusicUsecase = turnOffMusicUsecase;
        this.turnOnRemUsecase = turnOnRemUsecase;
        this.turnOffRemUsecase = turnOffRemUsecase;
    }


    @Override
    public void turnOnLight() {
        Log.d("bambi", "turnOnLight");
        turnOnLightUsecase.executeIO(new Subscriber() {
            @Override
            public void onCompleted() {
                Log.d("bambi", "turnOnLight completed");
            }

            @Override
            public void onError(Throwable e) {
                view.ConvertTextToSpeech("Đèn đã được mở");
                Log.d("bambi", "turnOnLight: " + e.toString());
            }

            @Override
            public void onNext(Object o) {
                Log.d("bambi", "turnOnLight onNext");
            }
        });
    }

    @Override
    public void turnOffLight() {
        turnOffLightUsecase.executeIO(new Subscriber() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {
                view.ConvertTextToSpeech("Đèn đã được tắt");
            }

            @Override
            public void onNext(Object o) {

            }
        });
    }

    @Override
    public void turnOnMusic() {
        turnOnMusicUsecase.executeIO(new Subscriber() {
            @Override
            public void onCompleted() {
                Log.d("bambi: turn on music", "onCompleted");
            }

            @Override
            public void onError(Throwable e) {
                Log.d("bambi: turn on music", e.toString());
                view.ConvertTextToSpeech("Nhạc đã được mở");
            }

            @Override
            public void onNext(Object o) {
                Log.d("bambi: turn on music", "onNext");
            }
        });
    }

    @Override
    public void turnOffMusic() {
        turnOffMusicUsecase.executeIO(new Subscriber() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {
                view.ConvertTextToSpeech("Nhạc đã được tắt");
            }

            @Override
            public void onNext(Object o) {

            }
        });
    }

    @Override
    public void turnOnRem() {
        turnOnRemUsecase.executeIO(new Subscriber() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {
                view.ConvertTextToSpeech("Thiết bị đã được mở");
            }

            @Override
            public void onNext(Object o) {
                view.ConvertTextToSpeech("Thiết bị đã được mở");
            }
        });
    }

    @Override
    public void turnOffRem() {
        turnOffRemUsecase.executeIO(new Subscriber() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {
                view.ConvertTextToSpeech("Thiết bị đã được đóng");
            }

            @Override
            public void onNext(Object o) {
                view.ConvertTextToSpeech("Thiết bị đã được đóng");
            }
        });
    }
}
