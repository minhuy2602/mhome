package com.demo.architect.presentation.screen.dialog;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.DrawableRes;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.SwitchCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;

@SuppressLint("UseSparseArrays")
public class AlertDialogFragment extends DialogFragment {

    private static final String TAG = "AlertDialogFragment";

    private static final String ARGUMENTS_TAG = "ARGUMENTS_TAG";
    private static final String ARGUMENTS_BUNDLE = "ARGUMENTS_BUNDLE";
    private static final String ARGUMENTS_LAYOUT_ID = "ARGUMENTS_LAYOUT_ID";
    private static final String ARGUMENTS_IMAGE_VIEWS = "ARGUMENTS_IMAGE_VIEWS";
    private static final String ARGUMENTS_TEXT_VIEWS = "ARGUMENTS_TEXT_VIEWS";
    private static final String ARGUMENTS_ACTION_VIEWS = "ARGUMENTS_ACTION_VIEWS";
    private static final String ARGUMENTS_SWITCH_VIEWS = "ARGUMENTS_SWITCH_VIEWS";
    private static final String ARGUMENTS_RATING_BARS = "ARGUMENTS_RATING_BARS";

    public static final String EXTRA_SWITCH_RESULTS = "EXTRA_SWITCH_RESULTS";
    public static final String EXTRA_RATING_BARS = "EXTRA_RATING_BARS";

    private String mTag;

    private int mLayoutId;

    private HashMap<Integer, Integer> mImageViews;


    private HashMap<Integer, String> mTextViews;

    private ArrayList<Integer> mActionViews;

    private ArrayList<Integer> mSwitchViews;

    private ArrayList<Integer> mRatingBars;

    private HashMap<Integer, Boolean> mSwitchResults;

    private HashMap<Integer, Float> mRatingResults;

    private Intent mData;

    private OnDialogFragmentResult mListener;

    private View mView;

    private static AlertDialogFragment newInstance(String tag,
                                                   Bundle bundle,
                                                   int layoutId,
                                                   HashMap<Integer, Integer> imageViews,
                                                   HashMap<Integer, String> textViews,
                                                   ArrayList<Integer> actionViews,
                                                   ArrayList<Integer> ratingBars,
                                                   ArrayList<Integer> switchViews) {
        AlertDialogFragment fragment = new AlertDialogFragment();
        Bundle arguments = new Bundle();
        arguments.putString(ARGUMENTS_TAG, tag);
        arguments.putBundle(ARGUMENTS_BUNDLE, bundle);
        arguments.putInt(ARGUMENTS_LAYOUT_ID, layoutId);
        arguments.putSerializable(ARGUMENTS_IMAGE_VIEWS, imageViews);
        arguments.putSerializable(ARGUMENTS_TEXT_VIEWS, textViews);
        arguments.putSerializable(ARGUMENTS_ACTION_VIEWS, actionViews);
        arguments.putSerializable(ARGUMENTS_SWITCH_VIEWS, switchViews);
        arguments.putSerializable(ARGUMENTS_RATING_BARS, ratingBars);
        fragment.setArguments(arguments);
        return fragment;
    }

    @SuppressWarnings("unchecked")
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setRetainInstance(true);

        mData = new Intent();
        Bundle arguments = getArguments();
        mTag = arguments.getString(ARGUMENTS_TAG);
        Bundle bundle = arguments.getBundle(ARGUMENTS_BUNDLE);
        if (bundle != null) {
            mData.putExtras(bundle);
        }
        mLayoutId = arguments.getInt(ARGUMENTS_LAYOUT_ID);
        mImageViews = (HashMap<Integer, Integer>) arguments.getSerializable(ARGUMENTS_IMAGE_VIEWS);
        mTextViews = (HashMap<Integer, String>) arguments.getSerializable(ARGUMENTS_TEXT_VIEWS);
        mActionViews = (ArrayList<Integer>) arguments.getSerializable(ARGUMENTS_ACTION_VIEWS);
        mSwitchViews = (ArrayList<Integer>) arguments.getSerializable(ARGUMENTS_SWITCH_VIEWS);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Fragment parentFragment = getParentFragment();
        if (parentFragment != null) {
            if (parentFragment instanceof OnDialogFragmentResult) {
                mListener = (OnDialogFragmentResult) parentFragment;
            }
        }
        if (mListener == null && context instanceof OnDialogFragmentResult) {
            mListener = (OnDialogFragmentResult) context;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(mLayoutId, container);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color
                .TRANSPARENT));
        return dialog;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mView = view;

        // set ImageViews
        if (mImageViews != null) {
            for (int viewId : mImageViews.keySet()) {
                ((ImageView) mView.findViewById(viewId)).setImageResource(mImageViews.get(viewId));
            }
        }

        // set TextViews
        if (mTextViews != null) {
            for (int viewId : mTextViews.keySet()) {
                ((TextView) mView.findViewById(viewId)).setText(mTextViews.get(viewId));
            }
        }

        // set Actions
        if (mActionViews != null) {
            for (final int viewId : mActionViews) {
                mView.findViewById(viewId).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dismiss();
                        if (mSwitchResults != null) {
                            mData.putExtra(EXTRA_SWITCH_RESULTS, mSwitchResults);
                        }
                        if (mRatingBars != null) {
                            mData.putExtra(EXTRA_RATING_BARS, mRatingBars);
                        }
                        if (mListener != null) {
                            mListener.onDialogFragmentResult(mTag, viewId, mData, mView);
                        }
                    }
                });
            }
        }

        // set RatingBars
        if (mRatingBars != null) {
            for (int viewId : mRatingBars) {
                if (mRatingResults == null) {
                    mRatingResults = new HashMap<>();
                }
                mRatingResults.put(viewId, 5f);
//                ((SwitchCompat) view.findViewById(viewId)).setOn);
            }
        }

        // set SwitchViews
        if (mSwitchViews != null) {
            for (int viewId : mSwitchViews) {
                if (mSwitchResults == null) {
                    mSwitchResults = new HashMap<>();
                }
                mSwitchResults.put(viewId, false);
                ((SwitchCompat) mView.findViewById(viewId)).setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton compoundButton, boolean isEnable) {
                        if (mSwitchResults == null) {
                            mSwitchResults = new HashMap<>();
                        }
                        mSwitchResults.put(compoundButton.getId(), isEnable);
                    }
                });
            }
        }

    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        if (mListener != null) {
            mListener.onDialogFragmentResult(mTag, 0, null, mView);
        }
    }

    @Override
    public void show(FragmentManager manager, @NonNull String tag) {

        // DialogFragment.show() will take care of adding the fragment
        // in a transaction.  We also want to delete any currently showing
        // dialog, so make our own transaction and take care of that here.
        FragmentTransaction ft = manager.beginTransaction();
        Fragment prev = manager.findFragmentByTag(tag);
        if (prev != null) {
            ft.remove(prev);
        }

        // Create and show the dialog.
        show(ft, tag);
    }

    public static class Builder {

        private int mLayoutId;

        private HashMap<Integer, Integer> mImageViews = new HashMap<>();

        private HashMap<Integer, String> mTextViews = new HashMap<>();

        private ArrayList<Integer> mActionViews = new ArrayList<>();

        private ArrayList<Integer> mSwitchViews = new ArrayList<>();

        private ArrayList<Integer> mRatingBars = new ArrayList<>();

        public Builder(int layoutId) {
            mLayoutId = layoutId;
        }

        public Builder addImageView(@IdRes int viewId, @DrawableRes int drawable) {
            mImageViews.put(viewId, drawable);
            return this;
        }

        public Builder addTextView(@IdRes int viewId, String text) {
            mTextViews.put(viewId, text);
            return this;
        }

        public Builder addActionView(@IdRes int viewId) {
            mActionViews.add(viewId);
            return this;
        }

        public Builder addSwitchView(@IdRes int viewId) {
            mSwitchViews.add(viewId);
            return this;
        }

        public Builder addRatingBar(@IdRes int viewId) {
            mRatingBars.add(viewId);
            return this;
        }

        public AlertDialogFragment create(String tag) {
            return create(tag, null);
        }

        public AlertDialogFragment create(String tag, Bundle arguments) {
            return AlertDialogFragment.newInstance(tag,
                    arguments,
                    mLayoutId,
                    mImageViews,
                    mTextViews,
                    mActionViews,
                    mRatingBars,
                    mSwitchViews);
        }

        public AlertDialogFragment show(FragmentManager manager, @NonNull String tag) {
            return show(manager, tag, null);
        }

        public AlertDialogFragment show(FragmentManager manager, @NonNull String tag, Bundle arguments) {
            final AlertDialogFragment dialog = create(tag, arguments);
            dialog.show(manager, tag);
            return dialog;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnDialogFragmentResult {
        void onDialogFragmentResult(String tag, int actionId, Intent data, View view);
    }
}
