package com.demo.architect.presentation.screen.tmp;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.speech.SpeechRecognizer;
import android.speech.tts.TextToSpeech;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.demo.architect.presentation.R;
import com.demo.architect.presentation.app.base.BaseFragment;
import com.demo.architect.presentation.app.di.Precondition;
import com.github.zagum.speechrecognitionview.RecognitionProgressView;
import com.github.zagum.speechrecognitionview.adapters.RecognitionListenerAdapter;
import com.google.gson.JsonElement;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import ai.api.AIServiceException;
import ai.api.android.AIConfiguration;
import ai.api.android.AIDataService;
import ai.api.model.AIRequest;
import ai.api.model.AIResponse;
import ai.api.model.Metadata;
import ai.api.model.Result;
import butterknife.ButterKnife;

/**
 * Created by admin on 7/14/17.
 */

public class TmpFragment extends BaseFragment implements TmpContract.View {

    private final static String TAG = TmpFragment.class.getName();
    private TmpContract.Presenter mPresenter;

    private static final int REQUEST_RECORD_AUDIO_PERMISSION_CODE = 1;

    private SpeechRecognizer speechRecognizer;
    private TextToSpeech speak;

    private Button den;
    private Button conversation;
    private Button music;
    private Button rem;
    private static int denCount = 0;
    private static int musicCount = 0;
    private static int conversationCount = 0;
    private static int remCount = 0;

    public static TmpFragment newInstance() {
        Bundle arguments = new Bundle();
        TmpFragment fragment = new TmpFragment();
        fragment.setArguments(arguments);
        return fragment;
    }

    public TmpFragment() {
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_recognize, container, false);
        ButterKnife.bind(this, view);

        den = (Button) view.findViewById(R.id.light);
        den.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (denCount % 2 == 0) {
                    mPresenter.turnOnLight();
                } else {
                    mPresenter.turnOffLight();
                }
                denCount++;
            }
        });

        conversation = (Button) view.findViewById(R.id.conversation);
        conversation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startRecognition();
            }
        });

        music = (Button) view.findViewById(R.id.music);
        music.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (musicCount % 2 == 0) {
                    mPresenter.turnOnMusic();
                } else {
                    mPresenter.turnOffMusic();
                }
                musicCount++;
            }
        });

        rem = (Button) view.findViewById(R.id.rem);
        rem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (remCount % 2 == 0) {
                    mPresenter.turnOnRem();
                } else {
                    mPresenter.turnOffRem();
                }
                remCount++;
            }
        });

        int[] colors = {
                ContextCompat.getColor(getActivity(), R.color.color1),
                ContextCompat.getColor(getActivity(), R.color.color2),
                ContextCompat.getColor(getActivity(), R.color.color3),
                ContextCompat.getColor(getActivity(), R.color.color4),
                ContextCompat.getColor(getActivity(), R.color.color5)
        };

        int[] heights = {20, 24, 18, 23, 16};

        speechRecognizer = SpeechRecognizer.createSpeechRecognizer(getActivity());

        final RecognitionProgressView recognitionProgressView = (RecognitionProgressView) view.findViewById(R.id.recognition_view);
        recognitionProgressView.setSpeechRecognizer(speechRecognizer);
        recognitionProgressView.setRecognitionListener(new RecognitionListenerAdapter() {
            @Override
            public void onResults(Bundle results) {
                showResults(results);
            }
        });
        recognitionProgressView.setColors(colors);
        recognitionProgressView.setBarMaxHeightsInDp(heights);
        recognitionProgressView.setCircleRadiusInDp(2);
        recognitionProgressView.setSpacingInDp(2);
        recognitionProgressView.setIdleStateAmplitudeInDp(2);
        recognitionProgressView.setRotationRadiusInDp(10);
        recognitionProgressView.play();

        Button listen = (Button) view.findViewById(R.id.listen);
        Button reset = (Button) view.findViewById(R.id.reset);

        listen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ContextCompat.checkSelfPermission(getActivity(),
                        Manifest.permission.RECORD_AUDIO)
                        != PackageManager.PERMISSION_GRANTED) {
                    requestPermission();
                } else {
                    startRecognition();
                    recognitionProgressView.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            startRecognition();
                        }
                    }, 50);
                }
            }
        });

        reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                recognitionProgressView.stop();
                recognitionProgressView.play();
            }
        });

        speak = new TextToSpeech(getActivity(), new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if (status != TextToSpeech.ERROR) {
                    speak.setLanguage(Locale.US);
                }
            }
        });

        speak = new TextToSpeech(getActivity(), new TextToSpeech.OnInitListener() {

            @Override
            public void onInit(int status) {
                // TODO Auto-generated method stub
                if (status == TextToSpeech.SUCCESS) {
                    int result = 0;
                    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                        result = speak.setLanguage(Locale.forLanguageTag("vi"));
                    }
                    if (result == TextToSpeech.LANG_MISSING_DATA ||
                            result == TextToSpeech.LANG_NOT_SUPPORTED) {
                        Log.e("error", "This Language is not supported");
                    } else {
                        ConvertTextToSpeech("em hôm xin được phục vụ quý khách.");
                    }

                    if (ContextCompat.checkSelfPermission(getActivity(),
                            Manifest.permission.RECORD_AUDIO)
                            != PackageManager.PERMISSION_GRANTED) {
                        requestPermission();
                    } else {
                        serviceForCustomer();
//            recognitionProgressView.postDelayed(new Runnable() {
//                @Override
//                public void run() {
//                    startRecognition();
//                }
//            }, 500);
                    }
                } else
                    Log.e("error", "Initilization Failed!");
            }
        });

        return view;
    }

    private void serviceForCustomer() {
        ConvertTextToSpeech("tôi có thể phục vụ gì cho bạn");
        startRecognition();
//        android.os.Handler handler=new android.os.Handler();
//        handler.postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                ConvertTextToSpeech("tôi có thể phục vụ gì cho bạn");
//                startRecognition();
//            }
//        },500);

//        recognitionProgressView.postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                startRecognition();
//            }
//        }, 500);
    }

    @Override
    public void ConvertTextToSpeech(String content) {
        // TODO Auto-generated method stub
        Log.d("bambi", "ConvertTextToSpeech: " + content);
        speak.speak(content, TextToSpeech.QUEUE_FLUSH, null);
    }

    private void checkAPISpeech(String data) {
        final AIConfiguration config1 = new AIConfiguration("b6b5ce0381364812b462e2ab2a0a888b",
                AIConfiguration.SupportedLanguages.English,
                AIConfiguration.RecognitionEngine.System);

        final AIDataService aiDataService = new AIDataService(getActivity(), config1);

        final AIRequest aiRequest = new AIRequest();
        aiRequest.setQuery(data);

        new AsyncTask<AIRequest, Void, AIResponse>() {

            private String requestContent = "";

            @Override
            protected AIResponse doInBackground(AIRequest... requests) {
                final AIRequest request = requests[0];
                try {
                    requestContent = aiRequest.toString();
                    final AIResponse response = aiDataService.request(aiRequest);
                    return response;
                } catch (AIServiceException e) {
                }
                return null;
            }

            @Override
            protected void onPostExecute(AIResponse response) {
                if (response != null) {
                    // process aiResponse here
                    Log.d("bambi", "onResult");

                    Log.d("bambi", requestContent);

//                    Toast.makeText(getApplicationContext(),response.getResult().toString(),Toast.LENGTH_SHORT).show();

//                    resultTextView.setText(gson.toJson(response));

                    Log.i("bambi", "Received success response");

                    // this is example how to get different parts of result object

                    final Result result = response.getResult();
                    Log.i("bambi", "Resolved query: " + result.getResolvedQuery());

                    Log.i("bambi", "Action: " + result.getAction());
                    final String speech = result.getFulfillment().getSpeech();
                    Log.i("bambi", "Speech: " + speech);
//                    TTS.speak(speech);

                    final Metadata metadata = result.getMetadata();
                    if (metadata != null) {
                        Log.i("bambi", "Intent id: " + metadata.getIntentId());
                        Log.i("bambi", "Intent name: " + metadata.getIntentName());
                        if (!metadata.getIntentName().equals("Default Fallback Intent")) {
                            Toast.makeText(getActivity(), speech, Toast.LENGTH_SHORT).show();
                        }
                    }

                    final HashMap<String, JsonElement> params = result.getParameters();
                    if (params != null && !params.isEmpty()) {
                        Log.i("bambi", "Parameters: ");
                        for (final Map.Entry<String, JsonElement> entry : params.entrySet()) {
                            Log.i("bambi", String.format("%s: %s", entry.getKey(), entry.getValue().toString()));
                        }
                    }
                }
            }
        }.execute(aiRequest);
    }

    @Override
    public void onDestroy() {
        if (speechRecognizer != null) {
            speechRecognizer.destroy();
        }
        super.onDestroy();
    }

    private void startRecognition() {
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_CALLING_PACKAGE, getActivity().getPackageName());
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, "vi");
        //        intent.putExtra(RecognizerIntent.EXTRA_SPEECH_INPUT_POSSIBLY_COMPLETE_SILENCE_LENGTH_MILLIS, 2000000);
        intent.putExtra(RecognizerIntent.EXTRA_SPEECH_INPUT_COMPLETE_SILENCE_LENGTH_MILLIS, new Long(5000));
//        intent.putExtra(RecognizerIntent.EXTRA_SPEECH_INPUT_MINIMUM_LENGTH_MILLIS, 20000000);
        speechRecognizer.startListening(intent);
    }

    private void showResults(Bundle results) {
        ArrayList<String> matches = results
                .getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION);
        Log.d("bambi", matches.get(0));
        if (matches.get(0).toLowerCase().contains("mở đèn") || matches.get(0).toLowerCase().contains("bật đèn")
                || matches.get(0).toLowerCase().contains("mo den") || matches.get(0).toLowerCase().contains("bat den")
                || matches.get(0).toLowerCase().contains("mở đi") || matches.get(0).toLowerCase().contains("bật đi")
                || matches.get(0).toLowerCase().contains("mo di") || matches.get(0).toLowerCase().contains("bat di")) {
            mPresenter.turnOnLight();
        } else if (matches.get(0).toLowerCase().contains("tắt đèn") || matches.get(0).toLowerCase().contains("đóng đèn")
                || matches.get(0).toLowerCase().contains("tat den") || matches.get(0).toLowerCase().contains("dong den")) {
            mPresenter.turnOffLight();
        } else if (matches.get(0).toLowerCase().contains("mở nhac") || matches.get(0).toLowerCase().contains("bật nhạc")
                || matches.get(0).toLowerCase().contains("mo nhac") || matches.get(0).toLowerCase().contains("bat nhac"))
        {
            mPresenter.turnOnMusic();
        } else
        if (matches.get(0).toLowerCase().contains("tắt nhạc") || matches.get(0).toLowerCase().contains("đóng nhạc")
                || matches.get(0).toLowerCase().contains("tat nhac") || matches.get(0).toLowerCase().contains("dong nhac")) {
            mPresenter.turnOffMusic();
        } else if (matches.get(0).toLowerCase().contains("mở rèm") || matches.get(0).toLowerCase().contains("bật rèm")
                || matches.get(0).toLowerCase().contains("mo rem") || matches.get(0).toLowerCase().contains("bat rem")
                || matches.get(0).toLowerCase().contains("mở rền") || matches.get(0).toLowerCase().contains("bật rền")
                || matches.get(0).toLowerCase().contains("mở cửa rèm") || matches.get(0).toLowerCase().contains("bật cửa rèm")
                || matches.get(0).toLowerCase().contains("mở gem") || matches.get(0).toLowerCase().contains("bật gem")
                || matches.get(0).toLowerCase().contains("mở ra xem") || matches.get(0).toLowerCase().contains("bật ra xem")) {
            mPresenter.turnOnRem();
        } else if (matches.get(0).toLowerCase().contains("tắt rèm") || matches.get(0).toLowerCase().contains("đóng rèm")
                || matches.get(0).toLowerCase().contains("tat rem") || matches.get(0).toLowerCase().contains("dong rem")) {
            mPresenter.turnOffRem();
        }
    }

    private void requestPermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                Manifest.permission.RECORD_AUDIO)) {
            Toast.makeText(getActivity(), "Requires RECORD_AUDIO permission", Toast.LENGTH_SHORT).show();
        } else {
            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{Manifest.permission.RECORD_AUDIO},
                    REQUEST_RECORD_AUDIO_PERMISSION_CODE);
        }
    }

    @Override
    public void setPresenter(TmpContract.Presenter presenter) {
        mPresenter = Precondition.checkNotNull(presenter);
    }

    @Override
    public void showProgressBar() {

    }

    @Override
    public void hideProgressBar() {

    }

}