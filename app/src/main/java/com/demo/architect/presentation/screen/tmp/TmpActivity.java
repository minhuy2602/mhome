package com.demo.architect.presentation.screen.tmp;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;

import com.demo.architect.presentation.R;
import com.demo.architect.presentation.app.CoreApplication;
import com.demo.architect.presentation.app.base.BaseActivity;
import com.demo.architect.presentation.app.di.Precondition;

import javax.inject.Inject;

/**
 * Created by admin on 7/14/17.
 */

public class TmpActivity extends BaseActivity {
    
    public static final String TAG = TmpActivity.class.getName();
    private static final String ID_BLOG = "ID_BLOG";

    @Inject
    TmpPresenter presenter;

    private TmpFragment fragment;
    private String id;

    public static void start(Context context, String id) {
        Intent intent = new Intent(context, TmpActivity.class);
        intent.putExtra(ID_BLOG, id);
        context.startActivity(intent);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base);

        initializeIntent();
        initializeFragment();
        initializeInjector(fragment);

        if (savedInstanceState != null) {
//            presenter.setId(savedInstanceState.getString(ID_BLOG));
        }

    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
//        outState.getString(ID_BLOG, presenter.getId());
        super.onSaveInstanceState(outState);
    }

    private void initializeIntent() {
        Intent intent = getIntent();
//        id = intent.getStringExtra(ID_BLOG);
    }

    private void initializeInjector(TmpFragment fragment) {
        CoreApplication.getInstance().getApplicationComponent().plus(new TmpModule(fragment)).inject(this);
    }

    private void initializeFragment() {
        fragment = (TmpFragment) getSupportFragmentManager().findFragmentById(R.id.fragmentContainer);
        if (fragment == null) {
            fragment = TmpFragment.newInstance();
            addFragmentToBackStack(fragment, R.id.fragmentContainer);
        }
    }

    protected void addFragmentToBackStack(Fragment fragment, int frameId) {
        Precondition.checkNotNull(fragment);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(frameId, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }
    
}
