package com.demo.architect.presentation.view;


import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.TextView;

import com.demo.architect.presentation.R;

/**
 * Created on 4/19/2017.
 */

public class StoreToolbar extends FrameLayout {

    private ImageButton btnMenu;
    private ImageButton btnCart;
    private TextView tvBadge;

    private StoreToolbarListener listener;

    public StoreToolbar(Context context) {
        super(context);
        init(context);
    }

    public StoreToolbar(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public StoreToolbar(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    public void init(Context context) {
        View root =inflate(context, R.layout.layout_store_toolbar, this);
        btnMenu = (ImageButton) root.findViewById(R.id.btn_menu);
        btnCart = (ImageButton) root.findViewById(R.id.btn_cart);
        tvBadge = (TextView) root.findViewById(R.id.tv_badge);

        setupButtonEvents();
    }

    private void setupButtonEvents() {
        btnMenu.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) {
                    listener.onMenuClick();
                }
            }
        });
        btnCart.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) {
                    listener.onCartClick();
                }
            }
        });
    }

    public void setBadge(int count) {
        tvBadge.setText(String.valueOf(count));
    }

    public void setToolbarListener(StoreToolbarListener listener) {
        this.listener = listener;
    }

    public interface StoreToolbarListener {
        void onMenuClick();
        void onCartClick();
    }
}
