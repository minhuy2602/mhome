package com.demo.architect.presentation.screen.wrapper;

/**
 * Created by admin on 4/25/17.
 */

public class SearchFilterWrapperItem {

    public final static int TYPE_ITEM = 0;
    public final static int TYPE_LOAD_MORE = 1;

    private int type;
    private int resId;
    private String title;
    private String id;

    public SearchFilterWrapperItem(String title) {
        this.title = title;
        this.type = TYPE_LOAD_MORE;
    }

    public SearchFilterWrapperItem(int resId, String title, String id) {
        this.resId = resId;
        this.title = title;
        this.id = id;
        this.type = TYPE_ITEM;
    }

    public static SearchFilterWrapperItem createSearchFilterWrapperItem(int resId, String title, String id) {
        return new SearchFilterWrapperItem(resId, title, id);
    }

    public static SearchFilterWrapperItem createSearchFilterLoadMoreWrapperItem(String title) {
        return new SearchFilterWrapperItem(title);
    }

    public int getResId() {
        return resId;
    }

    public void setResId(int resId) {
        this.resId = resId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }
}
