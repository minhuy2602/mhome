package com.demo.architect.presentation.app.di;

/**
 * Created by uyminhduc on 12/16/16.
 */

public interface HasComponent<C> {
    C getComponent();
}
