package com.demo.architect.presentation.app.di.module;

import com.demo.architect.data.repository.base.local.LocalRepository;
import com.demo.architect.data.repository.base.remote.RemoteRepository;
import com.demo.architect.data.repository.bill.remote.BillRepository;
import com.demo.architect.domain.usecase.demo.IMDBUsecase;
import com.demo.architect.domain.usecase.demo.TurnOffLightUsecase;
import com.demo.architect.domain.usecase.demo.TurnOffMusicUsecase;
import com.demo.architect.domain.usecase.demo.TurnOffRemUsecase;
import com.demo.architect.domain.usecase.demo.TurnOnLightUsecase;
import com.demo.architect.domain.usecase.demo.TurnOnMusicUsecase;
import com.demo.architect.domain.usecase.demo.TurnOnRemUsecase;

import dagger.Module;
import dagger.Provides;

/**
 * Created by uyminhduc on 12/16/16.
 */
@Module
public class UseCaseModule {
    public UseCaseModule() {
    }

    @Provides
    IMDBUsecase provideIMDBUsecase(BillRepository mApiRepository, LocalRepository localRepository) {
        return new IMDBUsecase(mApiRepository, localRepository);
    }

    @Provides
    TurnOnLightUsecase provideTurnOnLightUsecase(RemoteRepository remoteRepository){
        return new TurnOnLightUsecase(remoteRepository);
    }

    @Provides
    TurnOffLightUsecase provideTurnOffLightUsecase(RemoteRepository remoteRepository){
        return new TurnOffLightUsecase(remoteRepository);
    }

    @Provides
    TurnOnMusicUsecase provideTurnOnMusicUsecase(RemoteRepository remoteRepository){
        return new TurnOnMusicUsecase(remoteRepository);
    }

    @Provides
    TurnOffMusicUsecase provideTurnOffMusicUsecase(RemoteRepository remoteRepository){
        return new TurnOffMusicUsecase(remoteRepository);
    }

    @Provides
    TurnOnRemUsecase provideTurnOnRemUsecase(RemoteRepository remoteRepository){
        return new TurnOnRemUsecase(remoteRepository);
    }

    @Provides
    TurnOffRemUsecase provideTurnOffRemUsecase(RemoteRepository remoteRepository){
        return new TurnOffRemUsecase(remoteRepository);
    }
}

