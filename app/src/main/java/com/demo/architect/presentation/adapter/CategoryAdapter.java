package com.demo.architect.presentation.adapter;


import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.demo.architect.presentation.R;
import com.demo.architect.presentation.screen.wrapper.CategoryWrapperItem;

import java.util.List;

/**
 * Created on 4/21/2017.
 */

public class CategoryAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<CategoryWrapperItem> mCategories;

    public CategoryAdapter(List<CategoryWrapperItem> categories) {
        this.mCategories = categories;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View header = inflater.inflate(R.layout.item_category, parent, false);
        return new CategoryItemHolder(header);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return mCategories.size();
    }

    public class CategoryItemHolder extends RecyclerView.ViewHolder{

        public CategoryItemHolder(View itemView) {
            super(itemView);
        }
    }
}
