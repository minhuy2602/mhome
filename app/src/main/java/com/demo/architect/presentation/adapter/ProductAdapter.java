package com.demo.architect.presentation.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.demo.architect.presentation.R;
import com.demo.architect.presentation.screen.wrapper.ProductWrapperItem;

import java.util.ArrayList;


public class ProductAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private ArrayList<ProductWrapperItem> productList;
    private ProductItemListener listener;

    public ProductAdapter(ArrayList<ProductWrapperItem> productList, ProductItemListener listener) {
        this.productList = productList;
        this.listener = listener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.item_product, parent, false);
        return new ProductItemViewHolder(view, listener);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return productList != null ? productList.size() : 0;
    }

    public class ProductItemViewHolder extends RecyclerView.ViewHolder {

        public ImageView ivThumb;
        public TextView tvPrice;
        public TextView tvTitle;
        public TextView tvUnit;
        public TextView tvNote;
        public ProductItemListener listener;

        public ProductItemViewHolder(View itemView, ProductItemListener listener) {
            super(itemView);
            ivThumb = (ImageView) itemView.findViewById(R.id.iv_thumb);
            tvPrice = (TextView) itemView.findViewById(R.id.tv_price);
            tvTitle = (TextView) itemView.findViewById(R.id.tv_title);
            tvUnit = (TextView) itemView.findViewById(R.id.tv_unit);
            tvNote = (TextView) itemView.findViewById(R.id.tv_note);
            this.listener = listener;
        }
    }

    public interface ProductItemListener {

    }
}
