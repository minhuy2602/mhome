package com.demo.architect.presentation.screen.wrapper;

/**
 * Created on 4/21/2017.
 */

public class CategoryWrapperItem {
    private String id;
    private String thumb;
    private String title;

    public CategoryWrapperItem(String id, String thumb, String title) {
        this.id = id;
        this.thumb = thumb;
        this.title = title;
    }

    public static CategoryWrapperItem create(String id, String thumb, String title) {
        return new CategoryWrapperItem(id, thumb, title);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getThumb() {
        return thumb;
    }

    public void setThumb(String thumb) {
        this.thumb = thumb;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
