package com.demo.architect.presentation.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.demo.architect.presentation.R;
import com.demo.architect.presentation.screen.wrapper.ListProductWrapperItem;
import com.demo.architect.presentation.view.CustomizeListProductView;

import java.util.ArrayList;

/**
 * Created by admin on 4/25/17.
 */

public class CustomizeListProductViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private ArrayList<ListProductWrapperItem> productList;
    private CustomizeListProductViewListener listener;

    public CustomizeListProductViewAdapter(ArrayList<ListProductWrapperItem> productList, CustomizeListProductViewListener listener) {
        this.productList = productList;
        this.listener = listener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.item_main_product_list, parent, false);
        return new CustomizeListProductViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return productList != null ? productList.size() : 0;
    }

    public class CustomizeListProductViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public CustomizeListProductView vProductList;
        public CustomizeListProductViewListener listener;

        public CustomizeListProductViewHolder(View itemView) {
            super(itemView);
            vProductList = (CustomizeListProductView) itemView.findViewById(R.id.v_product_list);
        }

        @Override
        public void onClick(View v) {

        }
    }

    public interface CustomizeListProductViewListener {

    }
}
