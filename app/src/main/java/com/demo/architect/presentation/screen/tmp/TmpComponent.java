package com.demo.architect.presentation.screen.tmp;

import dagger.Subcomponent;

/**
 * Created by admin on 7/14/17.
 */

@Subcomponent(modules = TmpModule.class)
public interface TmpComponent {
    void inject(TmpActivity activity);
}
