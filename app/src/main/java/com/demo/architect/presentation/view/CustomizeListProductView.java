package com.demo.architect.presentation.view;

import android.content.Context;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.demo.architect.presentation.R;
import com.demo.architect.presentation.adapter.CustomizeListProductViewAdapter;
import com.demo.architect.presentation.adapter.ProductAdapter;
import com.demo.architect.presentation.screen.wrapper.ProductWrapperItem;

import java.util.ArrayList;

/**
 * Created by admin on 4/25/17.
 */

public class CustomizeListProductView extends RelativeLayout implements View.OnClickListener, ProductAdapter.ProductItemListener {

    private TextView tvTitle;
    private Button btLoadMore;
    private RecyclerView rvListProduct;
    private CustomizeListProductViewAdapter.CustomizeListProductViewListener listener;

    private String title;

    private ArrayList<ProductWrapperItem> productList;
    private ProductAdapter productAdapter;

    public CustomizeListProductView(Context context) {
        super(context);
        initViews(context);
    }

    public CustomizeListProductView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initViews(context);
    }

    public CustomizeListProductView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initViews(context);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public CustomizeListProductView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        initViews(context);
    }

    public String getTitle() {
        return title;
    }

    public ArrayList<ProductWrapperItem> getProductList() {
        return productList;
    }

    public void setProductList(ArrayList<ProductWrapperItem> productList) {
        this.productList = productList;
    }

    public ProductAdapter getProductAdapter() {
        return productAdapter;
    }

    public void setProductAdapter(ProductAdapter productAdapter) {
        this.productAdapter = productAdapter;
    }

    private void initViews(Context context) {
        View view =inflate(getContext(), R.layout.view_customize_list_product, this);

        tvTitle = (TextView) view.findViewById(R.id.tv_title);
        btLoadMore = (Button) view.findViewById(R.id.bt_loadmore);
        rvListProduct = (RecyclerView) view.findViewById(R.id.rv_list_product);

        productList = new ArrayList<>();

        for (int i = 0; i < 10; i++) {
            productList.add(ProductWrapperItem.createProductWrapperItem("", "", "", "", "", 1, 1, true, 1));
        }

        productAdapter = new ProductAdapter(productList, this);
        StaggeredGridLayoutManager staggeredGridLayoutManager = new StaggeredGridLayoutManager(1, LinearLayoutManager.HORIZONTAL);
        rvListProduct.setLayoutManager(staggeredGridLayoutManager);
        rvListProduct.setAdapter(productAdapter);
        productAdapter.notifyDataSetChanged();

    }

    @Override
    public void onClick(View v) {

    }
}
