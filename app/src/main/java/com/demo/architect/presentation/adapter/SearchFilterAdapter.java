package com.demo.architect.presentation.adapter;


import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.demo.architect.presentation.R;
import com.demo.architect.presentation.screen.wrapper.SearchFilterWrapperItem;

import java.util.ArrayList;

/**
 * Created by admin on 5/4/17.
 */

public class SearchFilterAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private ArrayList<SearchFilterWrapperItem> items;
    private SearchFilterItemListener listener;

    public SearchFilterAdapter(ArrayList<SearchFilterWrapperItem> items, SearchFilterItemListener listener) {
        this.items = items;
        this.listener = listener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        switch (viewType) {
            case SearchFilterWrapperItem.TYPE_ITEM: {
                View header = inflater.inflate(R.layout.item_search, parent, false);
                return new HeaderViewHolder(header);
            }
            default: {
                View header = inflater.inflate(R.layout.item_search_load_more, parent, false);
                return new HeaderViewHolder(header);
            }
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

    }

    @Override
    public int getItemViewType(int position) {
        return items.get(position).getType();
    }

    @Override
    public int getItemCount() {
        return items != null ? items.size() : 0;
    }

    public class SearchFilterItemViewHolder extends RecyclerView.ViewHolder {

        public SearchFilterItemViewHolder(View itemView) {
            super(itemView);
        }
    }

    public interface SearchFilterItemListener {

    }
}
