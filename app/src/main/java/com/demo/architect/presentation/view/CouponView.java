package com.demo.architect.presentation.view;

import android.content.Context;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.AttributeSet;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.demo.architect.presentation.R;
import com.demo.architect.presentation.adapter.CouponAdapter;
import com.demo.architect.presentation.screen.wrapper.CouponWrapperItem;

import java.util.ArrayList;

/**
 * Created by admin on 4/25/17.
 */

public class CouponView extends RelativeLayout {

    private TextView tvCouponNumber;
    private RecyclerView rvCouponList;

    private String title;
    private int number;

    private ArrayList<CouponWrapperItem> couponList;
    private CouponAdapter couponAdapter;

    public CouponView(Context context) {
        super(context);
        initViews();
    }

    public CouponView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initViews();
    }

    public CouponView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initViews();
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public CouponView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        initViews();
    }

    private void initViews() {
        View view = inflate(getContext(), R.layout.view_list_coupon, this);

        couponList = new ArrayList<>();
        couponList.add(CouponWrapperItem.createNumberCouponWrapperItem(""));
        for (int i = 0; i < 10; i++) {
            couponList.add(CouponWrapperItem.createCouponWrapperItem("", "", ""));
        }

        tvCouponNumber = (TextView) view.findViewById(R.id.tv_coupon_number);
        rvCouponList = (RecyclerView) view.findViewById(R.id.rv_coupon_list);

        couponAdapter = new CouponAdapter(couponList);
        StaggeredGridLayoutManager staggeredGridLayoutManager = new StaggeredGridLayoutManager(1, LinearLayoutManager.HORIZONTAL);
        rvCouponList.setLayoutManager(staggeredGridLayoutManager);
        rvCouponList.setAdapter(couponAdapter);
        couponAdapter.notifyDataSetChanged();
    }
}
