package com.demo.architect.presentation.app.di.component;

import com.demo.architect.presentation.app.CoreApplication;
import com.demo.architect.presentation.app.base.BaseActivity;
import com.demo.architect.presentation.app.base.BaseFragment;
import com.demo.architect.presentation.app.di.module.ApplicationModule;
import com.demo.architect.presentation.app.di.module.NetModule;
import com.demo.architect.presentation.app.di.module.RepositoryModule;
import com.demo.architect.presentation.app.di.module.UseCaseModule;
import com.demo.architect.presentation.screen.tmp.TmpComponent;
import com.demo.architect.presentation.screen.tmp.TmpModule;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by uyminhduc on 12/16/16.
 */

@Singleton
@Component(modules = {ApplicationModule.class,
        NetModule.class,
        UseCaseModule.class,
        RepositoryModule.class})
public interface ApplicationComponent {

    void inject(CoreApplication application);

    void inject(BaseActivity activity);

    void inject(BaseFragment fragment);

    TmpComponent plus(TmpModule tmpModule);
}
