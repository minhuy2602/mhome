package com.demo.architect.presentation.screen.tmp;

import dagger.Module;
import dagger.Provides;

/**
 * Created by admin on 7/14/17.
 */

@Module
public class TmpModule {
    private final TmpContract.View view;

    public TmpModule(TmpContract.View view) {
        this.view = view;
    }

    @Provides
    TmpContract.View provideLoginView() {
        return this.view;
    }

}
