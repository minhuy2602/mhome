package com.demo.architect.presentation.adapter;


import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.demo.architect.presentation.enums.MainViewType;
import com.demo.architect.presentation.R;
import com.demo.architect.presentation.screen.wrapper.MainWrapperItem;

import java.util.ArrayList;

public class MainAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private ArrayList<MainWrapperItem> items;

    public MainAdapter(ArrayList<MainWrapperItem> items) {
        this.items = items;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view;
        switch (MainViewType.getTypeFromInt(viewType)) {
            case HEADER: {
                view = inflater.inflate(R.layout.layout_header, parent, false);
                return new HeaderViewHolder(view);
            }
            case PRODUCT: {
                view = inflater.inflate(R.layout.item_main_product_list, parent, false);
                return new ProductListViewHolder(view);
            }
            case PROMOTION: {
                view = inflater.inflate(R.layout.item_main_promotion_list, parent, false);
                return new PromotionListViewHolder(view);
            }
            case COUPON: {
                view = inflater.inflate(R.layout.item_main_coupon_list, parent, false);
                return new CouponListViewHolder(view);
            }
            case INFORMATION: {
                view = inflater.inflate(R.layout.item_info, parent, false);
                return new InformationViewHolder(view);
            }
            default: {
                return null;
            }
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        switch (items.get(position).getType()) {
            case HEADER: {
                initHeaderViewHolder();
                break;
            }
            case PRODUCT: {
                initProductListViewHolder();
                break;
            }
            case PROMOTION: {
                initPromotionListViewHolder();
                break;
            }
            case COUPON: {
                initCouponListViewHolder();
                break;
            }
            case INFORMATION: {
                initInformationViewHolder();
                break;
            }
        }
    }

    private void initInformationViewHolder() {

    }

    private void initCouponListViewHolder() {

    }

    private void initPromotionListViewHolder() {

    }

    private void initProductListViewHolder() {

    }

    private void initHeaderViewHolder() {

    }

    @Override
    public int getItemViewType(int position) {
        return items.get(position).getType().getType();
    }

    @Override
    public int getItemCount() {
        return items != null ? items.size() : 0;
    }

    public class HeaderViewHolder extends RecyclerView.ViewHolder {



        public HeaderViewHolder(View itemView) {
            super(itemView);
        }
    }

    public class CouponListViewHolder extends RecyclerView.ViewHolder {

        public CouponListViewHolder(View itemView) {
            super(itemView);
        }
    }

    public class ProductListViewHolder extends RecyclerView.ViewHolder {

        public ProductListViewHolder(View itemView) {
            super(itemView);
        }
    }

    public class PromotionListViewHolder extends RecyclerView.ViewHolder {

        public PromotionListViewHolder(View itemView) {
            super(itemView);
        }
    }

    public class InformationViewHolder extends RecyclerView.ViewHolder {

        public InformationViewHolder(View itemView) {
            super(itemView);
        }
    }
}
