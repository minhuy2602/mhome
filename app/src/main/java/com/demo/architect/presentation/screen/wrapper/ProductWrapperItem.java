package com.demo.architect.presentation.screen.wrapper;

/**
 * Created by admin on 4/25/17.
 */

public class ProductWrapperItem {
    private String title;
    private String price;
    private String shortDescription;
    private String thumb;
    private String unit;
    private int status;
    private int id;
    private boolean isInCart;
    private int numberInCart;

    public ProductWrapperItem() {

    }

    public ProductWrapperItem(String title, String price, String shortDescription, String thumb, String unit, int status, int id, boolean isInCart, int numberInCart) {
        this.title = title;
        this.price = price;
        this.shortDescription = shortDescription;
        this.thumb = thumb;
        this.status = status;
        this.id = id;
        this.isInCart = isInCart;
        this.numberInCart = numberInCart;
        this.unit = unit;
    }

    public static ProductWrapperItem createProductWrapperItem(String title, String price, String shortDescription, String thumb, String unit, int status, int id, boolean isInCart, int numberInCart) {
        return new ProductWrapperItem(title, price, shortDescription, thumb, unit, status, id, isInCart, numberInCart);
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getShortDescription() {
        return shortDescription;
    }

    public void setShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
    }

    public String getThumb() {
        return thumb;
    }

    public void setThumb(String thumb) {
        this.thumb = thumb;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isInCart() {
        return isInCart;
    }

    public void setInCart(boolean inCart) {
        isInCart = inCart;
    }

    public int getNumberInCart() {
        return numberInCart;
    }

    public void setNumberInCart(int numberInCart) {
        this.numberInCart = numberInCart;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }
}
