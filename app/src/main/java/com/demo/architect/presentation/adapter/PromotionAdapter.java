package com.demo.architect.presentation.adapter;


import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.demo.architect.presentation.R;
import com.demo.architect.presentation.screen.wrapper.PromotionWrapperItem;

import java.util.ArrayList;

/**
 * Created by admin on 4/25/17.
 */

public class PromotionAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private ArrayList<PromotionWrapperItem> items;

    public PromotionAdapter(ArrayList<PromotionWrapperItem> items) {
        this.items = items;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View header = inflater.inflate(R.layout.item_promotion, parent, false);
        return new HeaderViewHolder(header);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return items != null ? items.size() : 0;
    }

    public class PromotionItemViewHolder extends RecyclerView.ViewHolder {

        public PromotionItemViewHolder(View itemView) {
            super(itemView);
        }
    }

}