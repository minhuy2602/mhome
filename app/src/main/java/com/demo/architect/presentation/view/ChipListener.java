package com.demo.architect.presentation.view;

public interface ChipListener {
  void chipCheckedChange(int index, boolean checked, boolean userClick);
}
