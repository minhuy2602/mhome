package com.demo.architect.presentation.view;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.demo.architect.presentation.R;

/**
 * Created on 4/19/2017.
 */

public class FloatStoreToolbar extends CardView {

    private ImageButton btnMenu;
    private ImageButton btnCart;
    private Button btnSearch;
    private TextView tvBadge;

    private FloatStoreToolbarListener listener;

    public FloatStoreToolbar(Context context) {
        super(context);
        init(context);
    }

    public FloatStoreToolbar(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public FloatStoreToolbar(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    public void init(Context context) {
        View root =inflate(context, R.layout.layout_store_toolbar_float, this);
        btnMenu = (ImageButton) root.findViewById(R.id.btn_menu);
        btnCart = (ImageButton) root.findViewById(R.id.btn_cart);
        btnSearch = (Button) root.findViewById(R.id.btn_search);
        tvBadge = (TextView) root.findViewById(R.id.tv_badge);

        setupButtonEvents();
    }

    private void setupButtonEvents() {
        btnMenu.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) {
                    listener.onMenuClick();
                }
            }
        });
        btnCart.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) {
                    listener.onCartClick();
                }
            }
        });
        btnSearch.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) {
                    listener.onSearchClick();
                }
            }
        });
    }

    public void setBadge(int count) {
        tvBadge.setText(String.valueOf(count));
    }

    public void setToolbarListener(FloatStoreToolbarListener listener) {
        this.listener = listener;
    }

    public interface FloatStoreToolbarListener {
        void onMenuClick();
        void onCartClick();
        void onSearchClick();
    }
}
