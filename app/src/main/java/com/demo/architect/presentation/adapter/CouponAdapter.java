package com.demo.architect.presentation.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.demo.architect.presentation.R;
import com.demo.architect.presentation.screen.wrapper.CouponWrapperItem;

import java.util.ArrayList;


/**
 * Created by admin on 4/25/17.
 */

public class CouponAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private ArrayList<CouponWrapperItem> items;

    public CouponAdapter(ArrayList<CouponWrapperItem> items) {
        this.items = items;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        switch (viewType) {
            case CouponWrapperItem.TYPE_NUMBER:
                View header = inflater.inflate(R.layout.item_number_coupon, parent, false);
                return new HeaderViewHolder(header);
            default:
                View row = inflater.inflate(R.layout.item_coupon, parent, false);
                return new HeaderViewHolder(row);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return items != null ? items.size() : 0;
    }

    @Override
    public int getItemViewType(int position) {
        return items.get(position).getType();
    }

    public class CouponItemViewHolder extends RecyclerView.ViewHolder {

        public CouponItemViewHolder(View itemView) {
            super(itemView);
        }
    }

    public class CouponNumberViewHolder extends RecyclerView.ViewHolder {

        public CouponNumberViewHolder(View itemView) {
            super(itemView);
        }
    }
}
