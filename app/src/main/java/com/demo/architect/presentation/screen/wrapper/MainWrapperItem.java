package com.demo.architect.presentation.screen.wrapper;

import android.view.View;

import com.demo.architect.presentation.enums.MainViewType;

import java.util.ArrayList;

/**
 * Created by admin on 4/30/17.
 */

public class MainWrapperItem {
    private MainViewType type;
    private String id;
    private String title;
    private String thumb;
    private String cover;
    private String shortDescription;
    private String price;
    private String status;
    private String note;
    private String unit;
    private boolean isInCart;
    private String number;
    private boolean isCloseButton;
    private boolean isLoadMoreButton;

    private ArrayList<ProductWrapperItem> productList;
    private ArrayList<CouponWrapperItem> couponList;
    private ArrayList<PromotionWrapperItem> promotionList;

    private View.OnClickListener clickListener;

    public MainWrapperItem(MainViewType type) {
        this.type = type;
    }

    public static MainWrapperItem createShopHeader(String title, String thumb, String cover, String shortDescription) {
        MainWrapperItem item = new MainWrapperItem(MainViewType.HEADER);

        item.title = title;
        item.thumb = thumb;
        item.cover = cover;
        item.shortDescription = shortDescription;

        return item;
    }

    public static MainWrapperItem createInformation(String title, String shortDescription, boolean isCloseButton) {
        MainWrapperItem item = new MainWrapperItem(MainViewType.INFORMATION);

        item.title = title;
        item.shortDescription = shortDescription;
        item.isCloseButton = isCloseButton;

        return item;
    }

    public static MainWrapperItem createProductList(String id, String title, boolean isLoadMoreButton, ArrayList<ProductWrapperItem> productList) {
        MainWrapperItem item = new MainWrapperItem(MainViewType.PRODUCT);

        item.id = id;
        item.title = title;
        item.isLoadMoreButton = isLoadMoreButton;
        item.productList = productList;

        return item;
    }

    public static MainWrapperItem createCouponList(String id, String title, boolean isLoadMoreButton, ArrayList<CouponWrapperItem> couponList) {
        MainWrapperItem item = new MainWrapperItem(MainViewType.COUPON);

        item.id = id;
        item.title = title;
        item.isLoadMoreButton = isLoadMoreButton;
        item.couponList = couponList;

        return item;
    }

    public static MainWrapperItem createPromotionList(String id, String title, boolean isLoadMoreButton, ArrayList<PromotionWrapperItem> promotionList) {
        MainWrapperItem item = new MainWrapperItem(MainViewType.PROMOTION);

        item.id = id;
        item.title = title;
        item.isLoadMoreButton = isLoadMoreButton;
        item.promotionList = promotionList;

        return item;
    }

    public MainViewType getType() {
        return type;
    }

    public String getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getThumb() {
        return thumb;
    }

    public String getCover() {
        return cover;
    }

    public String getShortDescription() {
        return shortDescription;
    }

    public String getPrice() {
        return price;
    }

    public String getStatus() {
        return status;
    }

    public String getNote() {
        return note;
    }

    public String getUnit() {
        return unit;
    }

    public boolean isInCart() {
        return isInCart;
    }

    public String getNumber() {
        return number;
    }

    public boolean isCloseButton() {
        return isCloseButton;
    }

    public boolean isLoadMoreButton() {
        return isLoadMoreButton;
    }

    public ArrayList<ProductWrapperItem> getProductList() {
        return productList;
    }

    public ArrayList<CouponWrapperItem> getCouponList() {
        return couponList;
    }

    public ArrayList<PromotionWrapperItem> getPromotionList() {
        return promotionList;
    }

    public View.OnClickListener getClickListener() {
        return clickListener;
    }
}
