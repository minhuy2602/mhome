package com.demo.architect.data.repository.base.remote;

import com.demo.architect.data.model.BaseResponse;
import com.demo.architect.data.model.IMDBEntity;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;

/**
 * Created by uyminhduc on 10/16/16.
 */

public interface RemoteApiInterface {

    @GET("http://www.imdb.com/xml/find?json=1&nr=1&nm=on&q=jeniffer+garner")
    Call<IMDBEntity> getIMDB();

    @Headers({
            "Authorization: Basic a3l0aHVhdEBraW1zb250aWVuLmNvbTpDaG90cm9ubmllbXZ1aTE="
    })
//    @FormUrlEncoded
    @POST("http://192.168.1.10:80/api/scenes/72/action/start")
    Call<BaseResponse> turnOnLight();

    @Headers({
            "Authorization: Basic a3l0aHVhdEBraW1zb250aWVuLmNvbTpDaG90cm9ubmllbXZ1aTE="
    })
//    @FormUrlEncoded
    @POST("http://192.168.1.10:80/api/scenes/73/action/start")
    Call<BaseResponse> turnOffLight();

    @Headers({
            "Authorization: Basic a3l0aHVhdEBraW1zb250aWVuLmNvbTpDaG90cm9ubmllbXZ1aTE="
    })
    //    @FormUrlEncoded
    @POST("http://192.168.1.10:80/api/scenes/94/action/start")
    Call<BaseResponse> turnOnMusic();

    @Headers({
            "Authorization: Basic a3l0aHVhdEBraW1zb250aWVuLmNvbTpDaG90cm9ubmllbXZ1aTE="
    })
//    @FormUrlEncoded
    @POST("http://192.168.1.10:80/api/scenes/83/action/start")
    Call<BaseResponse> turnOffMusic();

    @Headers({
            "Authorization: Basic a3l0aHVhdEBraW1zb250aWVuLmNvbTpDaG90cm9ubmllbXZ1aTE="
    })
//    @FormUrlEncoded
    @POST("http://192.168.1.10:80/api/devices/157/action/turnOn")
    Call<BaseResponse> turnOnRem();

    @Headers({
            "Authorization: Basic a3l0aHVhdEBraW1zb250aWVuLmNvbTpDaG90cm9ubmllbXZ1aTE="
    })
//    @FormUrlEncoded
    @POST("http://192.168.1.10:80/api/devices/157/action/turnOff")
    Call<BaseResponse> turnOffRem();
}
