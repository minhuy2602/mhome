package com.demo.architect.data.repository.base.remote;

import com.demo.architect.data.model.BaseResponse;
import com.demo.architect.data.model.IMDBEntity;
import com.demo.architect.data.model.MessageModel;

//import javax.inject.Singleton;

import java.util.List;

import retrofit2.Call;
import rx.Observable;
import rx.Subscriber;

/**
 * Created by uyminhduc on 10/16/16.
 */
//@Singleton
public class RemoteRepositoryImpl implements RemoteRepository {

    private final static String TAG = RemoteRepositoryImpl.class.getName();

    private RemoteApiInterface mRemoteApiInterface;

    public RemoteRepositoryImpl(RemoteApiInterface mRemoteApiInterface) {
        this.mRemoteApiInterface = mRemoteApiInterface;
    }

    private void handleIMDBEntityResponse(Call<IMDBEntity> call, Subscriber subscriber) {
        try {
            IMDBEntity response = call.execute().body();
            if (!subscriber.isUnsubscribed()) {
                if (response != null) {
                    subscriber.onNext(response);
                } else {
                    subscriber.onError(new Exception("Network Error!"));
                }
                subscriber.onCompleted();
            }
        } catch (Exception e) {
            if (!subscriber.isUnsubscribed()) {
                subscriber.onError(e);
                subscriber.onCompleted();
            }
        }
    }

    @Override
    public Observable<IMDBEntity> getIMDB() {
        return Observable.create(new Observable.OnSubscribe<IMDBEntity>() {
            @Override
            public void call(Subscriber<? super IMDBEntity> subscriber) {
                handleIMDBEntityResponse(mRemoteApiInterface.getIMDB(), subscriber);
            }
        });
    }

    @Override
    public Observable<String> add(MessageModel model) {
        return null;
    }

    @Override
    public Observable<List<MessageModel>> findAll() {
        return null;
    }

    private void turnOffLight(Call<BaseResponse> call, Subscriber subscriber) {
        try {
            BaseResponse response = call.execute().body();
            if (!subscriber.isUnsubscribed()) {
                if (response != null) {
                    subscriber.onNext(response);
                } else {
                    subscriber.onError(new Exception("Network Error!"));
                }
                subscriber.onCompleted();
            }
        } catch (Exception e) {
            if (!subscriber.isUnsubscribed()) {
                subscriber.onError(e);
                subscriber.onCompleted();
            }
        }
    }

    private void turnOnLight(Call<BaseResponse> call, Subscriber subscriber) {
        try {
            BaseResponse response = call.execute().body();
            if (!subscriber.isUnsubscribed()) {
                if (response != null) {
                    subscriber.onNext(response);
                } else {
                    subscriber.onError(new Exception("Network Error!"));
                }
                subscriber.onCompleted();
            }
        } catch (Exception e) {
            if (!subscriber.isUnsubscribed()) {
                subscriber.onError(e);
                subscriber.onCompleted();
            }
        }
    }

    private void turnOnMusic(Call<BaseResponse> call, Subscriber subscriber) {
        try {
            BaseResponse response = call.execute().body();
            if (!subscriber.isUnsubscribed()) {
                if (response != null) {
                    subscriber.onNext(response);
                } else {
                    subscriber.onError(new Exception("Network Error!"));
                }
                subscriber.onCompleted();
            }
        } catch (Exception e) {
            if (!subscriber.isUnsubscribed()) {
                subscriber.onError(e);
                subscriber.onCompleted();
            }
        }
    }

    private void turnOffMusic(Call<BaseResponse> call, Subscriber subscriber) {
        try {
            BaseResponse response = call.execute().body();
            if (!subscriber.isUnsubscribed()) {
                if (response != null) {
                    subscriber.onNext(response);
                } else {
                    subscriber.onError(new Exception("Network Error!"));
                }
                subscriber.onCompleted();
            }
        } catch (Exception e) {
            if (!subscriber.isUnsubscribed()) {
                subscriber.onError(e);
                subscriber.onCompleted();
            }
        }
    }

    private void turnOffRem(Call<BaseResponse> call, Subscriber subscriber) {
        try {
            BaseResponse response = call.execute().body();
            if (!subscriber.isUnsubscribed()) {
                if (response != null) {
                    subscriber.onNext(response);
                } else {
                    subscriber.onError(new Exception("Network Error!"));
                }
                subscriber.onCompleted();
            }
        } catch (Exception e) {
            if (!subscriber.isUnsubscribed()) {
                subscriber.onError(e);
                subscriber.onCompleted();
            }
        }
    }

    private void turnOnRem(Call<BaseResponse> call, Subscriber subscriber) {
        try {
            BaseResponse response = call.execute().body();
            if (!subscriber.isUnsubscribed()) {
                if (response != null) {
                    subscriber.onNext(response);
                } else {
                    subscriber.onError(new Exception("Network Error!"));
                }
                subscriber.onCompleted();
            }
        } catch (Exception e) {
            if (!subscriber.isUnsubscribed()) {
                subscriber.onError(e);
                subscriber.onCompleted();
            }
        }
    }

    @Override
    public Observable<BaseResponse> turnOnLight() {
        return Observable.create(new Observable.OnSubscribe<BaseResponse>() {
            @Override
            public void call(Subscriber<? super BaseResponse> subscriber) {
                turnOnLight(mRemoteApiInterface.turnOnLight(), subscriber);
            }
        });
    }

    @Override
    public Observable<BaseResponse> turnOffLight() {
            return Observable.create(new Observable.OnSubscribe<BaseResponse>() {
                @Override
                public void call(Subscriber<? super BaseResponse> subscriber) {
                    turnOffLight(mRemoteApiInterface.turnOffLight(), subscriber);
                }
            });
    }

    @Override
    public Observable<BaseResponse> turnOnMusic() {
        return Observable.create(new Observable.OnSubscribe<BaseResponse>() {
            @Override
            public void call(Subscriber<? super BaseResponse> subscriber) {
                turnOffLight(mRemoteApiInterface.turnOnMusic(), subscriber);
            }
        });
    }

    @Override
    public Observable<BaseResponse> turnOffMusic() {
        return Observable.create(new Observable.OnSubscribe<BaseResponse>() {
            @Override
            public void call(Subscriber<? super BaseResponse> subscriber) {
                turnOffLight(mRemoteApiInterface.turnOffMusic(), subscriber);
            }
        });
    }

    @Override
    public Observable<BaseResponse> turnOnRem() {
        return Observable.create(new Observable.OnSubscribe<BaseResponse>() {
            @Override
            public void call(Subscriber<? super BaseResponse> subscriber) {
                turnOffLight(mRemoteApiInterface.turnOnRem(), subscriber);
            }
        });
    }

    @Override
    public Observable<BaseResponse> turnOffRem() {
        return Observable.create(new Observable.OnSubscribe<BaseResponse>() {
            @Override
            public void call(Subscriber<? super BaseResponse> subscriber) {
                turnOffLight(mRemoteApiInterface.turnOffRem(), subscriber);
            }
        });
    }
}

